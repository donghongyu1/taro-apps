import React, { Component } from 'react'
import Taro from '@tarojs/taro'
import { connect } from 'react-redux'
import { View, Button, Text } from '@tarojs/components'

import { add, minus, asyncAdd } from '../../actions/counter'

import './index.scss'

// #region 书写注意
//
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

type PageStateProps = {
  counter: {
    num: number
  }
}

type PageDispatchProps = {
  add: () => void
  dec: () => void
  asyncAdd: () => any
}

type PageOwnProps = {}


type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface Index {
  props: IProps;
}

@connect(({ counter }) => ({
  counter
}), (dispatch) => ({
  add () {
    dispatch(add())
    
  },
  dec () {
    dispatch(minus())
  },
  asyncAdd () {
    dispatch(asyncAdd())
  }
}))
class Index extends Component {
  componentWillReceiveProps (nextProps) {
    console.log(this.props, nextProps)
  }

  componentWillUnmount () { }

  componentDidShow () { 
    // 基础类型
    let isDone: boolean = false;                          //布尔
    let decLiteral: number = 6;                           //数字
    let name: string = "bob";                             //字符串
    let sentence: string = `Hello, my name is ${ name }` //字符串
    let list: number[] = [1, 2, 3];                      //数组
    let list1: Array<number> = [1, 2, 3];                //Array<元素类型>：
    let x: [string, number];                              //元组 Tuple 表示一个已知元素数量和类型的数组，各元素的类型不必相同
    enum Color {Red, Green, Blue}                         //枚举
    let c: Color = Color.Green;
   // 默认情况下，从0开始为元素编号。 你也可以手动的指定成员的数值。 例如，我们将上面的例子改成从 1开始编号：
   enum Color1 {Red = 1, Green, Blue}
   let c1: Color = Color.Green;
   let notSure: any = 4;            //Any 为那些在编程阶段还不清楚类型的变量指定一个类型。 这些值可能来自于动态的内容，
   notSure = "maybe a string instead";
   notSure = false;               
   
  }

  componentDidHide () { }

  render () {
    return (
      <View className='index'>
        
        <View><Text>Hello, World</Text></View>
      </View>
    )
  }
}

export default Index

